package com.example.examencorte1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView lblNombre;
    private EditText txtNombre;
    private Button btnEntrar;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblNombre = (TextView)findViewById(R.id.lblNombre);
        txtNombre = (EditText)findViewById(R.id.txtNombre);
        btnEntrar = (Button)findViewById(R.id.btnEntrar);
        btnSalir = (Button)findViewById(R.id.btnSalir);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar()){
                    String nombre = txtNombre.getText().toString();
                    Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
                    intent.putExtra("nombre",nombre);
                    startActivity(intent);
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public boolean validar(){
        boolean b=true;
        if(txtNombre.getText().toString().trim().matches("")){
            Toast.makeText(MainActivity.this,"Por favor introduzca su nombre",Toast.LENGTH_SHORT).show();
            b= false;
        }
        return b;
    }
}
