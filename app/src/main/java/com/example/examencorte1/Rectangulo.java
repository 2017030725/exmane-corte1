package com.example.examencorte1;

public class Rectangulo {
    private int base;
    private int altura;

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public Rectangulo() {
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public float calcularArea(){
        return (this.base * this.altura)/2;
    }

    public float calcularPerimtero(){
        return (this.base + this.base) + (this.altura + this.altura);
    }
}
