package com.example.examencorte1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {

    private TextView lblNombre;

    private TextView lblBase;
    private EditText txtBase;

    private TextView lblAltura;
    private EditText txtAltura;

    private TextView lblArea;
    private EditText txtArea;

    private TextView lblPerimetro;
    private EditText txtPerimetro;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        lblNombre = (TextView)findViewById(R.id.lblMinombre);

        lblBase = (TextView)findViewById(R.id.lblBase);
        txtBase = (EditText)findViewById(R.id.txtBase);

        lblAltura = (TextView)findViewById(R.id.lblAltura);
        txtAltura = (EditText)findViewById(R.id.txtAltura);

        lblArea = (TextView)findViewById(R.id.lblArea);
        txtArea = (EditText)findViewById(R.id.txtArea);

        lblPerimetro = (TextView)findViewById(R.id.lblPerimetro);
        txtPerimetro = (EditText)findViewById(R.id.txtPerimetro);

        btnCalcular = (Button)findViewById(R.id.btnCalcular);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnRegresar = (Button)findViewById(R.id.btnRegresar);

        Bundle datos =getIntent().getExtras();
        lblNombre.setText(datos.getString("nombre"));

        rectangulo = new Rectangulo();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar()){
                    calcular();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void limpiar(){
        txtBase.setText("");
        txtAltura.setText("");
        txtArea.setText("");
        txtPerimetro.setText("");
        txtBase.requestFocus();
    }

    public boolean validar(){
        boolean b=true;
        if(txtBase.getText().toString().trim().matches("")){
            Toast.makeText(RectanguloActivity.this,"Por favor introduzca un valor para la base",Toast.LENGTH_SHORT).show();
            txtBase.requestFocus();
            b=false;
        }
        else if(txtAltura.getText().toString().trim().matches("")){
            Toast.makeText(RectanguloActivity.this,"Por favor introduzca un valor para la altura",Toast.LENGTH_SHORT).show();
            txtAltura.requestFocus();
            b=false;
        }

        return b;
    }

    public void calcular(){
        int base = Integer.parseInt(txtBase.getText().toString().trim());
        rectangulo.setBase(base);
        int altura = Integer.parseInt(txtAltura.getText().toString().trim());
        rectangulo.setAltura(altura);

        txtPerimetro.setText(String.valueOf(rectangulo.calcularPerimtero()));
        txtArea.setText(String.valueOf(rectangulo.calcularArea()));
    }
}
